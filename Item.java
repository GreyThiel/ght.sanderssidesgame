
/**
 * Things and items used in the game.
 *
 * @author Grey Thiel
 * @version 02.19.2021
 */
public class Item
{
    private String name, type, description;
    private int damage, uses;

    /**
     * Default constructor for objects of class Item
     */
    public Item()
    {
        name = null;
        type = null;
        description = null;
        damage = -1;
        uses = 0;
        
        System.out.println("This item is still in default form.");
    }
    
    /**
     * Specific constructor for objects of class Item
     */
    public Item(String n, String t, String d, int weaponType)
    {
        name = n;
        type = t;
        description = d;
        
        switch(weaponType)
        {
            case 0:
               damage = 0;
               uses = -1;
               break;
            case 1:
               damage = 10;
               uses = -1;
               break;
            case 2:
                damage = 20;
                uses = 15;
                break;
            case 3:
                damage = 30;
                uses = 13;
                break;
            case 4:
                damage = 40;
                uses = 11;
                break;  
        }
    }

    /**
     * Getters and setters
     */
    public String getName()
    {
        return name;
    }
    
    public String getType()
    {
        return type;
    }
    
    public String getDescription()
    {
        return description;
    }
    
    public int getDamage()
    {
        return damage;
    }
    
    public int getUses()
    {
        return uses;
    }
    
    public void setName(String n)
    {
        name = n;
    }
    
    public void setType(String t)
    {
        type = t;
    }
    
    public void setDescription(String d)
    {
        description = d;
    }
    
    public void setDamage(int weaponType)
    {
        switch(weaponType)
        {
            case 0:
               damage = 0;
               break;
            case 1:
               damage = 10;
               break;
            case 2:
                damage = 20;
                break;
            case 3:
                damage = 30;
                break;
            case 4:
                damage = 40;
                break;
        }
    }
    
    public void setUses(int u)
    {
        uses = u;
    }
}
