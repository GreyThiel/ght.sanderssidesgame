
/**
 * Load and hold data for the first level of the game.
 *
 * @author Grey Thiel
 * @version 02.19.2021
 */
public class LevelOne
{
    private Item red_key, blue_key, silver_key, gold_key, green_key, purple_key, rainbow_key, rusty_key; //keys
    private Item old_painting, painting_one, painting_two, painting_three, painting_four, painting_five; //paintings
    private Item knife, axe, ornate_dagger; //weapons
    private Item sketch_book, locket, letter, book, album, photo; //clues
    
    private Room basement, foyer, theater, dressing_room, closet, dining_room, drawing_room, kitchen, library;
    private Room master_bedroom, bedroom, garden, nursery, courtyard, ballroom, stairs, attic;
    private Room hall_one, hall_two, hall_three, hall_four, hall_five;
    
    private Character logan, patton, virgil, thomas, janus, remus, roman;
    /**
     * Constructor for class LevelOne.
     */
    public LevelOne()
    {
        //characters
         logan = new Character("Logan", "Glass", "Logan is neat, smart, and knows what he wants. His isn't physically imposing, but " +
         "his gaze is enough to make even a grown adult nervous. He is 25 and is not known to have any romantic partners." +
         " He has dark brown hair, piercing blue eyes, and a calm disposition.", 150);
         
         patton = new Character("Patton", "Hart", "Patton is soft in every sense of the word. Constantly smiling, he works at the"
         + " house as the cook, making delicious meals. He's 23 and loves everyone, though he has no one he calls his partner" +
         ". He has sky blue eyes, wavy brown hair and freckles dotting his face.", 150);
         
         virgil = new Character("Virgil", "Black", "Virgil is like a shadow. He moves through the building swiftly like a shadow" +
         "He tells things as they are, and his anxiety won't let him get to close to anyone. Recently, however, he has been getting" +
         " closer to the house manager. He has grey eyes, black hair, and wears dark smudges under his eyes.", 150);
         
         thomas = new Character("Thomas", "Sanders", "Surrounded by a pool of blood, Thomas, the manor's owner, lies with several" +
         " stab wounds in his chest. Although blood cakes his hair and face, you can tell that his hair was warm brown." +
         " His warm brown eyes stare up at you blankly. How could someone kill such a kind man?", 0);
         thomas.setAlive(false);
         
         janus = new Character("Janus", "Declan", "Janus has scars covering most of the right side of his body after suffering "
         + "burns in a fire. Now, he runs the house with an iron will and keeps the staff in line. Nothing happens in the " +
         "manor that escapes his golden eyes, but even when his long, black hair falls in his eyes.", 150);
         
         remus = new Character("Remus", "Griswald", "Remus is a bit of a loose cannon. He loves working outdoors managing" +
         " the manor grounds. Dirt smudges his freckled cheeks and dusts his auburn hair, but that doesn't keep his green eyes" +
         "from sparkling with mischief and joy.", 150);
         
         roman = new Character("Roman", "Reyes", "Roman is an actor and entertainer as well as close friends with the owner." +
         " He loves the theater and his physique compliments his dream of acting well. Handsome, kind, and confident, Roman " +
         "takes fantastic care of his fiery red hair and enhances his emerald green eyes with makeup.", 150);
         
         //rooms
         basement = new Room(null,  null, closet, null, "The basement is filled with dust, crates, and an old painting. A half-open" +
         " door rests in the east wall. Its dark, but not dark enough to lower your vision.", null);
         basement.addItem(old_painting);
         
         foyer = new Room(basement, theater, dining_room, null, "The foyer is grand. The front door is locked tightly no matter" +
         " how hard you try to open it. The north end of the room holds the door to the basement. Heavy double doors rest on" +
         " the south wall. A less grand archway leads into what looks from here to be a dining room on the east wall.", null);
         foyer.addItem(red_key);
         
         theater = new Room(foyer, null, drawing_room, null, "", null);
         theater.addItem(sketch_book);
         theater.addItem(gold_key);
         
         dressing_room = new Room(theater, null, null, null, "", roman);
         
         closet = new Room(null, null, null, basement, "",null);
         closet.addItem(rusty_key);
         closet.addItem(axe);
         
         dining_room = new Room(null, drawing_room, null, foyer, "", null);
         dining_room.addItem(silver_key);
         
         drawing_room = new Room(dining_room, null, hall_three, theater, "", null);
         drawing_room.addItem(blue_key);
         drawing_room.addItem(letter);
         
         kitchen = new Room(null, dining_room, hall_one, null, "", patton);
         kitchen.addItem(knife);
         
         library = new Room(null, null, hall_two, dining_room, "", logan);
         library.addItem(book);
         
         master_bedroom = new Room(null, null, hall_four, null, "", thomas);
         
         bedroom = new Room(null, null, hall_five, null, "", null);
         bedroom.addItem(locket);
         bedroom.addItem(rainbow_key);
         bedroom.addItem(ornate_dagger);
         
         garden = new Room(null, null, null, hall_one, "", remus);
         
         nursery = new Room(null, null, null, hall_two, "", null);
         nursery.addItem(album);
         
         courtyard = new Room(null, null, null, hall_three, "", virgil);
         
         ballroom = new Room(null, null, null, hall_four, "", janus);
         
         stairs = new Room(null, null, attic, hall_five, "", null);
         
         attic = new Room(null, null, null, stairs, "", null);
         attic.addItem(photo);
         attic.addItem(purple_key);
         
         hall_one = new Room(null, hall_two, null, null, "", null);
         hall_one.addItem(painting_one);
         
         hall_two = new Room(hall_one, hall_three, nursery, null, "", null);
         hall_two.addItem(painting_two);
         
         hall_three = new Room(hall_two, hall_four, null, null, "", null);
         hall_three.addItem(painting_three);
         
         hall_four = new Room(hall_three, hall_five, null, null, "", null);
         hall_four.addItem(painting_four);
         
         hall_five = new Room(hall_four, null, stairs, bedroom, "", null);
         hall_five.addItem(painting_five);
         hall_five.addItem(green_key);
         
         //keys
         red_key = new Item("Red Key", "key", "The red key is shiny and sleek. Real gold is inlayed in its surface, " +
         "swirling around a green gem.", 0);
         
         blue_key = new Item("Blue Key", "key", "Matte and simple, this blue key lacks any extravagence.", 0);
         
         silver_key = new Item("Silver Key", "key", "As well as being pure silver, this key has heart shaped markings on it.", 0);
         
         gold_key = new Item("Gold Key", "key", "Ornate and gaudy, you see gold rubbing off in your hands as you hold it. Clearly" +
         " the key was not real gold.", 0);
         
         green_key = new Item("Green Key", "key", "The green key is twisted like the roots of a tree. A red apple hangs from one of" +
         " its branches.", 0);
         
         purple_key = new Item("Purple Key", "key", "This purple key is sleek and shiny with thin spiderwebs etched into the shaft.", 0);
         
         rainbow_key = new Item("Rainbow Key", "key", "Despite its complex colors, this key is the simplest of the keys." +
         " It also appears to be rusting at the end.", 0);
         
         rusty_key = new Item("Rusty Key", "key", "Flakes of rust crumble off its surface. You aren't sure if it still works.", 0);
         
         //clues
         sketch_book = new Item("Sketch Book", "clue", "The cover is plain and simple. Inside are many skilled drawings done in" +
         " charcoal and pencil. Most are of flowers and insects, but the later ones are of a man with long hair in various" +
         " styles.", 1);
         
         locket = new Item("Locket", "clue", "The front of the gold locket has the letter \"L\" engraved in it surrounded" + 
         " by daisies and roses. Inside is the painting of a young man with wavy, brown hair smiling happily." +
         " The reverse side has a little message engraved: \"Love you sweetly, like fresh made cookies.\"", 0);
         
         letter = new Item("Letter", "clue", "\"Dear P, You know I love you, but you also know that what we have," +
         " though beautiful, simply and sadly cannot be. I'm not leaving you, I promise. It is my opinion, however" +
         " that we keep this relationship a secret. I know this hurts you, as it does me, but if the world found out" +
         " it could ruin us both. Some day it will change. It has to. Yours forever, L\"", 0);
         
         book = new Item("Book", "clue", "A black-covered book with lined paper inside. Notes are scrawled inside in cursive." +
         " A quick flip through reveals that the notes are about the house and its goings on. Whoever owns this knows a lot of" +
         " secrets.", 1);
         
         album = new Item("Album", "clue", "A heavy book filled with pictures of the same boys from the paintings in the hall." +
         " One shows the older one on his first date. Another a somber funeral. You can just make out that it is for the" +
         " older brother's husband. One more shows the younger brother with several diplomas in History, Literature, and" +
         " Chemistry from a prestigious university.", 1);
         
         photo = new Item("Photograph", "clue", "The faded photograph shows two boys. They are clearly twins, but one is clean" +
         " while the other brother looks as if he enjoys playing in the mud outside.", 0);
         
         //paintings
         old_painting = new Item("Old Painting", "painting", "Because the painting is covered in a thick layer" +
         " of dust and is badly water damaged, you can't really make out what it is of. The best you can guess" +
         " is that it is an elderly couple.", 0);
         
         painting_one = new Item("Baby Painting", "painting", "The painting depicts two baby boys. One of them looks" +
         " about a year older than the other. Both babies are smiling, happy, and healthy. There isn't much else to" +
         " the painting.", 0);
         
         painting_two = new Item("Toddler Painting", "painting", "This painting shows two toddler boys. One of them is playing" + 
         " happily with little wooden toys, while the other is flipping through a picture book. Both boys look very happy.", 0);
         
         painting_three = new Item("Child Painting", "painting", "The painting shows two young boys enjoying the outdoors" +
         " in vastly different ways. The older of the two is chasing after a butterfly with daisies in his hand" +
         " while the other is relaxing in the shade of an oak tree, reading happily.", 0);
         
         painting_four = new Item("Young Adult Painting", "painting", "The older brother in this painting proudly shows off his" +
         " diploma from a nearby high-ranking university. The younger one hugs a stack of books on various topics to" +
         " his chest, giving the painter a half smile.", 0);
         
         painting_five = new Item("Wedding Painting", "painting", "A happy young many stands proudly with another man. The painting" +
         " was clearly painted for a wedding and neither man could look happier.", 0);
         
         //weapons
         axe = new Item("Axe", "weapon", "A simple woodcutting axe.", 3);
         
         knife = new Item("Knife", "weapon", "A stainless steel kitchen knife with a wooden handle.", 2);
         
         ornate_dagger = new Item("Ornate Dagger", "special", "The blade is made of hand-sharpened quartz. The handle is rich redwood" +
         " with gold decorations embedded.", 4);
         
    }

    public void play()
    {
        System.out.println("Level One Started.");
    }
}
