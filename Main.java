
/**
 * Runs the playing of the game as a whole.
 *
 * @author Grey Thiel
 * @version 02.20.2021
 */
public class Main
{
    private LevelOne levelOne;
    private LevelTwo levelTwo;

    /**
     * Constructor for objects of class Main
     */
    public Main()
    {
        levelOne = new LevelOne();
        levelTwo = new LevelTwo();
    }
    
    public void start()
    {
        System.out.println("The Tradegy of Sanders Hill");
        System.out.println("Programming by: MorallyGrey Games");
        System.out.println("Characters by: Thomas Sanders");
        
        levelOne.play();
    }
    
    public void end()
    {
        System.out.println("Characters alternate universe versions of characters by Thomas Sanders from the web-series, Sanders' Sides.");
        System.out.println("A special thanks goes to my high school programming teacher, Edwin Rusch, for teaching me" +
        " Java programming and my friend, Payton Ness, for encouraging me to start programming in the first place.");
        System.out.println("More thanks goes to you, the player, for playing my game. I do hope you enjoyed!");
        System.out.println("Game written inside the BlueJ Java compiler.");
        
        //this.quit()  :: this is not the right method
    }
}
