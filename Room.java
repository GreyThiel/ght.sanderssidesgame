import java.util.ArrayList;
/**
 * The class Room is used for different rooms or places in the game.
 *
 * @author Grey Thiel
 * @version 02.18.2021
 */
public class Room
{
    Room north, south, east, west;
    String description;
    ArrayList<Item> roomItems;
    Character roomCharacter;

    /**
     * Default constructor for objects of class Room
     */
    public Room()
    {
        north = null;
        south = null;
        east = null;
        west = null;
        description = null;
        roomItems = new ArrayList<Item>();
        roomCharacter = new Character();
        
        System.out.println("This room is still in default form.");
    }
    
    /**
     * Specific constructor for objects of class Room
     */
    public Room(Room northRoom, Room southRoom, Room eastRoom, Room westRoom, String lDesc, Character character)
    {
        north = northRoom;
        south = southRoom;
        east = eastRoom;
        west = westRoom;
        description = lDesc;
        roomItems = new ArrayList<Item>();
        roomCharacter = character;
    }

    /**
     * getters and setters
     */
    public Room getNorth()
    {
        return north;
    }
    
    public Room getSouth()
    {
        return south;
    }
    
    public Room getEast()
    {
        return east;
    }
    
    public Room getWest()
    {
        return west;
    }
    
    public String getDescription()
    {
        return description;
    }
    
    public ArrayList<Item> getItems()
    {
        return roomItems;
    }
    
    public Character getCharacter()
    {
        return roomCharacter;
    }
    
    public void setNorth(Room room)
    {
        north = room;
    }
    
    public void setSouth(Room room)
    {
        south = room;
    }
    
    public void setEast(Room room)
    {
        east = room;
    }
    
    public void setWest(Room room)
    {
        west = room;
    }
    
    public void setDescription(String desc)
    {
        description = desc;
    }
    
    public void addItem(Item item)
    {
        roomItems.add(item);
    }
    
    public void addCharacter(Character character)
    {
        roomCharacter = character;
    }
}
