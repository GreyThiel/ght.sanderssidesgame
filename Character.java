import java.util.ArrayList;
/**
 * An NPC character in the story.
 *
 * @author Grey Thiel
 * @version 02.18.2021
 */
public class Character
{
    private String firstName, lastName, description;
    private int hitPoints;
    private boolean alive;
    /**
     * Default constructor for objects of class Character
     */
    public Character()
    {
        firstName = null;
        lastName = null;
        description = null;
        hitPoints = -1;
        alive = false;
        
        System.out.println("This character is still in default form.");
    }
    
    /**
     * Specific constructor for objects of class Character
     */
    public Character(String fname, String lname, String d, int hp)
    {
        firstName = fname;
        lastName = lname;
        description = d;
        hitPoints = hp;
        alive = true;
    }
    
    /**
     * 
     */
    public String talk(String word)
    {
        String phrase = "";
        
        if(word == "greeting")
        {
            phrase = "Hello! My name is " + firstName + " " + lastName + ".";
        }
        else if(word == "hurt")
        {
            if(firstName == "Logan")
            {
                phrase = "Ow.";
            }
            else if(firstName == "Patton")
            {
                phrase = "Ouch!";
            }
            else if(firstName == "Virgil")
            {
                phrase = "Hey! Watch it!";
            }
            else if(firstName == "Janus")
            {
                phrase = "Oof.";
            }
            else if(firstName == "Roman")
            {
                phrase = "Hey!";
            }
            else if(firstName == "Remus")
            {
                phrase = "Ow!";
            }
            else if(firstName == "Thomas")
            {
                phrase = "Ow, that hurt!";
            }
        }
        else if(word == "scared")
        {
            if(firstName == "Logan")
            {
                phrase = "I... I don't know about this.";
            }
            else if(firstName == "Patton")
            {
                phrase = "Hey... this is bad...";
            }
            else if(firstName == "Virgil")
            {
                phrase = "I don't like this. I don't like this. I don't like this.";
            }
            else if(firstName == "Janus")
            {
                phrase = "Everything is just fine! Fine and dandy!";
            }
            else if(firstName == "Roman")
            {
                phrase = "God, this is awful.";
            }
            else if(firstName == "Remus")
            {
                phrase = "I really don't like this at all.";
            }
            else if(firstName == "Thomas")
            {
                phrase = "Yea... I don't like this.";
            }
        }
        
        return phrase;
    }

    /**
     * Setters and getters
     */
    public String getFirstName()
    {
        return firstName;
    }
    
    public String getLastName()
    {
        return lastName;
    }
    
    public String getDescription()
    {
        return description;
    }
    
    public int getHitPoints()
    {
        return hitPoints;
    }
    
    public Boolean isAlive()
    {
        return alive;
    }
    
    public void setFirstName(String fname)
    {
        if(fname != null)
        {
            firstName = fname;
        }
        else
        {
            System.out.println("");
            System.out.println("First Name cannot be null.");
            System.out.println("");
        }
    }
    
    public void setLastName(String lname)
    {
        if(lname != null)
        {
            lastName = lname;
        }
        else
        {
            System.out.println("");
            System.out.println("Last Name cannot be null.");
            System.out.println("");
        }
    }
    
    public void setDescription(String d)
    {
        description = d;
    }
    
    public void setHitPoints(int hp, char action)
    {
        if(hp <= -1)
        {
            if(action == '+')
            {
                hitPoints += hp;
            }
            else if(action == '-')
            {
                hitPoints -= hp;
                if(hitPoints <= 0)
                {
                    setAlive(false);
                }
            }
            else
            {
                hitPoints = hp;
            }
        }
        else
        {
            System.out.println("");
            System.out.println("Hit points cannot be null.");
            System.out.println("");
        }
    }
    
    public void setAlive(boolean a)
    {
        alive = a;
    }
}
